<?php

declare(strict_types=1);
// src/Service/Anagram.php
namespace App\Service;

class Anagram
{
    private static function prepareWordForCheck(string $word): string
    {
        $word = preg_replace('/\s*/', '', $word);
        $word = strtolower($word);

        return $word;
    }

    private static function findUniqueChars($word): array
    {
        $length = strlen($word);
        $uniqueChars = array();

        for ($i = 0; $i < $length; $i++) {
            $char = $word[$i];

            if (isset($uniqueChars[$char])) {
                $uniqueChars[$char]++;
            } else {
                $uniqueChars[$char] = 1;
            }
        }

        return $uniqueChars;
    }

    public static function check(string $word1, string $word2): bool
    {
        $word1 = Anagram::prepareWordForCheck($word1);
        $word2 = Anagram::prepareWordForCheck($word2);

        if (strlen($word1) != strlen($word2)) {
            return false;
        }

        $word1UniqueChars = Anagram::findUniqueChars($word1);

        $length = strlen($word2);

        for ($i = 0; $i < $length; $i++) {
            $char = $word2[$i];

            if (isset($word1UniqueChars[$char])) {
                if ($word1UniqueChars[$char] == 0) {
                    return false;
                }
                $word1UniqueChars[$char]--;
            } else {
                return false;
            }
        }

        return true;
    }
}
