<?php

declare(strict_types=1);
// src/Command/DeactivatePassiveUsersCommand.php
namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class DeactivatePassiveUsersCommand extends Command
{
    protected static $defaultName = 'app:deactivate-passive-users';
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Deativating inactive users.')
            ->setHelp('This command deactivates users that have not logged in for a month.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $userRepo = $this->entityManager->getRepository(User::class);

        $usersToDisable = $userRepo->findNotDisabledInactiveUsers();

        foreach ($usersToDisable as $user) {
            $user->setActive(false);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        $outputStyle = new OutputFormatterStyle('green');
        $output->getFormatter()->setStyle('green', $outputStyle);
        $output->writeln('<green>Inactive users found and deactivated: ' . count($usersToDisable) . '</>');

        return Command::SUCCESS;
    }
}
