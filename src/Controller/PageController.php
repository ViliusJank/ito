<?php

declare(strict_types=1);
// src/Controller/PageController.php
namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Form\AnagramType;
use App\Service\Anagram;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(): Response
    {

        return $this->render('pages/front_page.html.twig');
    }

    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcome(): Response
    {

        return $this->render('pages/welcome.html.twig');
    }

    /**
     * @Route("/anagram", name="anagram_solver", methods={"GET","POST"})
     */
    public function anagramSolver(Request $request): Response
    {
        $form = $this->createForm(AnagramType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->get('anagram');

            $word1 = $data['word_one'];
            $word2 = $data['word_two'];

            $isAnagram = Anagram::check($word1, $word2);
            return $this->render('pages/anagram_solver.html.twig', [
                'form' => $form->createView(),
                'isAnagram' => $isAnagram,
            ]);
        }

        return $this->render('pages/anagram_solver.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
