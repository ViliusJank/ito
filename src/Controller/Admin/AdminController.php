<?php

declare(strict_types=1);
// src/Controller/AdminController.php
namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $usersCount = $this->getDoctrine()
            ->getRepository(User::class)
            ->count([]);

        $siteInfo = [
            'User count' => $usersCount,
            'Database URL' => array_key_exists("DATABASE_URL", $_ENV) ? $_ENV['DATABASE_URL'] : 'Not specified',
            'Mailer URL' => array_key_exists("MAILER_URL", $_ENV) ? $_ENV['MAILER_URL'] : 'Not specified',
            'Mailer DSN' => array_key_exists("MAILER_DSN", $_ENV) ? $_ENV['MAILER_DSN'] : 'Not specified',
            'App environment' => array_key_exists("APP_ENV", $_ENV) ? $_ENV['APP_ENV'] : 'Not specified',
        ];

        return $this->render('admin/dashboard.html.twig', [
            'siteInfo' => $siteInfo,
        ]);
    }
}
