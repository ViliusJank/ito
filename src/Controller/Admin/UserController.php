<?php

declare(strict_types=1);
// src/Controller/UserController.php
namespace App\Controller\Admin;

use App\Entity\User;
use App\Event\UserRegisteredEvent;
use App\EventSubscriber\UserSubscriber;
use App\Form\ChangePasswordType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Twig\Environment;

/**
 * @Route("/admin")
 */
class UserController extends AbstractController
{
    private $dispatcher;
    private $encoder;
    private $emailVerifier;

    public function __construct(
        EmailVerifier $emailVerifier,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->dispatcher = new EventDispatcher();
        $this->emailVerifier = $emailVerifier;
        $this->encoder = $encoder;
    }

    /**
     * @Route("/user", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/create", name="user_create", methods={"GET","POST"})
     */
    public function create(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $plainPassword = $user->getPassword();
            $encoded = $this->encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);

            $entityManager->persist($user);
            $entityManager->flush();

            $userSubscriber = new UserSubscriber($this->emailVerifier); // TODO: Temp. Fix. Inject services through setter injection.
            $this->dispatcher->addSubscriber($userSubscriber);

            $event = new UserRegisteredEvent($user);
            $this->dispatcher->dispatch($event, UserRegisteredEvent::NAME);

            $this->addFlash(
                'notice',
                'User created successfully.'
            );

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/create.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'submitLabel' => 'Create user',
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user)->remove('password');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                'User changes saved.'
            );

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'User deleted successfully.'
            );
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/user/{id}/changePassword", name="user_change_password", methods={"GET","POST"})
     */
    public function changePassword(Request $request, User $user): Response
    {
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $plainPassword = $user->getPassword();
            $encoded = $this->encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                'User password changed successfully.'
            );

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/change_password.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
