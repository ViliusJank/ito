<?php

declare(strict_types=1);
// src/Controller/RegistrationController.php
namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Event\UserRegisteredEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Twig\Environment;
use App\EventSubscriber\UserSubscriber;
use App\Security\EmailVerifier;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private $dispatcher;
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
        $this->dispatcher = new EventDispatcher();
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $userSubscriber = new UserSubscriber($this->emailVerifier); // TODO: Temp. Fix. Inject services through setter injection.
            $this->dispatcher->addSubscriber($userSubscriber);

            $event = new UserRegisteredEvent($user);
            $this->dispatcher->dispatch($event, UserRegisteredEvent::NAME);

            $this->addFlash(
                'notice',
                'User created successfully.'
            );

            return $this->redirectToRoute('login');
        }

        return $this->render('registration/register.html.twig', [
            'form' => $form->createView(),
            'submitLabel' => 'Register',
        ]);
    }

    /**
     * @Route("/verify/email", name="email_verification")
     */
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        if ($user instanceof \Symfony\Component\Security\Core\User\User) {
            $this->addFlash('error', 'This user cannot confirm email, login with the created user.');

            return $this->redirectToRoute('home');
        }

        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('error', $exception->getReason());

            return $this->redirectToRoute('register');
        }

        $this->addFlash('notice', 'Your email address has been verified.');

        return $this->redirectToRoute('login');
    }
}
