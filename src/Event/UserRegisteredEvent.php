<?php

declare(strict_types=1);
// src/Entity/UserRegisteredEvent.php
namespace App\Event;

use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The user.registered event is dispatched each time a user registers successfully
 */
class UserRegisteredEvent extends Event
{
    public const NAME = 'user.registered';

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
