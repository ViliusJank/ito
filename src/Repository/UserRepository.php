<?php

declare(strict_types=1);
// src/Repository/UserRepository.php
namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }


    public function findNotDisabledInactiveUsers(): ?array
    {
        $usersNotNew = $this->createQueryBuilder('user')
            ->where('user.active = 1')
            ->andWhere('user.created_at < :monthBeforeTodayDate')
            ->setParameter('monthBeforeTodayDate', new \DateTime('-1 month'))
            ->getQuery()
            ->getResult();

        $activeUsers = $this->createQueryBuilder('user')
            ->andWhere('user.active = 1')
            ->Join('user.events', 'event', 'WITH', 'event.created_at > :deactivationDate') // TODO get black list param working.
            ->setParameter('deactivationDate', new \DateTime('-1 month'))
            ->getQuery()
            ->getResult();

        $inactiveUsers = array();

        foreach ($usersNotNew as $user) {
            $userIsActive = false;
            foreach ($activeUsers as $activeUser) {
                if ($user->getEmail() == $activeUser->getEmail()) {
                    $userIsActive = true;
                }
            }

            if (!$userIsActive) {
                array_push($inactiveUsers, $user);
            }
        }

        return $inactiveUsers;
    }
}
