# iTo Project

First time trying Symfony, I was able to spent a couple of days on this project,
that's not a lot of time to spend learning a framework, in my opinion. Lets take that into consideration.
Really looking forward into improving further if given the chance:)


## Prerequisites

I am running my system on:
PHP 7.3.18
NPM 6.9.0
Composer 1.8.6
Yarn 1.22.4

## Env file setup

Set these fields:
1. DATABASE_URL
2. MAILER_URL
3. MAILER_DSN

## Installation

1. composer install
2. yarn install
3. yarn encore dev
4. php bin/console doctrine:database:create
5. php bin/console doctrine:migrations:migrate
6. Enjoy

## Testing the user deactivation command

To test:
1. Pick a user,
2. Make all user login events more than a month old
3. Make user creation date at least a month old.

The command "php bin/console app:deactivate-passive-users" should deactivate the user.

## Author

* **Vilius Jankūnas**
